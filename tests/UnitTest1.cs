using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tostil.Tools.Queues;
using Tostil.Tools.Queues.AmazonSqs;

namespace tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var settings = new QueueAdapterSettings() { DataConnectionString = "QueueUrl=https://sqs.us-east-1.amazonaws.com/607144685156/teseo-prod-outbound-sqs-queue;AccessKey=AKIAIHRYLFWGO6XIUQPQ;SecretKey=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" };
            var ops = Microsoft.Extensions.Options.Options.Create(settings);
            var adapter1 = new AmazonSqsQueueAdapter(ops);
            var adapter2 = new AmazonSqsQueueAdapter(ops);


            adapter1.Push(new QueueMessageBase("ping"));

            var msg = adapter2.Pop();
            Assert.IsFalse(msg.IsEmpty);
            Assert.IsTrue(msg.Content.Contains("ping"));

            adapter1.Push(new QueueMessageBase("ping2"));

            msg = adapter2.Pop();
            Assert.IsFalse(msg.IsEmpty);
            Assert.IsTrue(msg.Content.Contains("ping2"));
        }
    }
}

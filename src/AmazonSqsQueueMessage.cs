﻿using Amazon.SQS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tostil.Tools.Queues.AmazonSqs
{
    public class AmazonSqsQueueMessage : QueueMessageBase 
    {
        private Message _message;

        public Message QueueMessage
        {
            get { return _message; }
        }

         #region constructors
        public AmazonSqsQueueMessage() : base()
        { }

        internal AmazonSqsQueueMessage(Message message)            
        {            
            var toSet = string.Empty;
            if (message != null && !string.IsNullOrEmpty(message.Body.ToString()))
            {
                toSet = message.Body.ToString();
            }
            this._content = toSet;
            this._message = message;
        }

        public AmazonSqsQueueMessage(string message)
            : base(message)
        {
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using Amazon.SQS;
using Microsoft.Extensions.Options;
using Amazon.Runtime;
using Amazon.SQS.Model;

namespace Tostil.Tools.Queues.AmazonSqs
{
    public class AmazonSqsQueueAdapter : QueueAdapterBase
    {
        private AmazonSQSClient _client;
        private string _queueUrl;
        private string _accessKey;
        private string _secretKey;

        #region Constructors
        public AmazonSqsQueueAdapter(IOptions<QueueAdapterSettings> settings)
            : base(settings)
        {
            InitializeElements();
        }

        public void InitializeElements()
        {
            _client = GetClient();
        }
        #endregion

        #region Private Methods
        private AmazonSQSClient GetClient()
        {                        
            if (_client == null)
            {
                // dataConectionString format :
                // QueueUrl=xxxxxxxx;AccessKey=yyyyyyyyyyyy;SecretKey=zzzzzzzzzzzzzzz

                var dataConnectionStringParts = _settings.DataConnectionString.Split(";").ToList();
                
                foreach (var part in dataConnectionStringParts)
                {
                    var keyValueParts = part.Split("=");
                    switch (keyValueParts[0].ToUpper())
                    {
                        case "QUEUEURL":
                            _queueUrl = keyValueParts[1];
                            break;

                        case "ACCESSKEY":
                            _accessKey = keyValueParts[1];
                            break;

                        case "SECRETKEY":
                            _secretKey = keyValueParts[1];
                            break;
                    }
                }

                //Create some Credentials with our IAM user
                var awsCreds = new BasicAWSCredentials(_accessKey, _secretKey);

                //Create a client to talk to SQS
                var region = Amazon.RegionEndpoint.APNortheast1;
                if (_queueUrl.Contains("us-east-1"))
                {
                    region = Amazon.RegionEndpoint.USEast1;
                }

                return new AmazonSQSClient(awsCreds, region);

            }

            return _client;                
        }
        #endregion

        protected override bool PushInternal(IQueueMessage item)
        {
            var success = true;            
            var client = GetClient();

            var sendRequest = new SendMessageRequest();
            sendRequest.QueueUrl = _queueUrl;
            sendRequest.MessageBody = item.Content;

            try
            {
                var sendMessageResponse = client.SendMessageAsync(sendRequest).Result;
            }
            catch (Exception ex)
            {
                success = false;
            }            

            return success;
        }

        protected override IQueueMessage PopInternal()
        {
            var message = GetInternal();
            if (message != null && !message.IsEmpty)
            {
                DeleteInternal(message);
            }

            return message; 
        }

        protected override IQueueMessage GetInternal()
        {
            AmazonSqsQueueMessage toReturn = new AmazonSqsQueueMessage();

            #region Get the message
            var client = GetClient();
            var receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.QueueUrl = _queueUrl;
            List<Message> messageReceived = new List<Message>();
            ReceiveMessageResponse response = null;

            try
            {
                response = client.ReceiveMessageAsync(receiveMessageRequest).Result;
            }
            catch (Exception ex)
            {
            }
            #endregion

            #region Parse 
            if (response != null && response.Messages.Any())
            {
                if (response.Messages.Count == 1)
                {
                    toReturn = new AmazonSqsQueueMessage(response.Messages[0]);
                }
            }
            #endregion

            return toReturn;
        }

        protected override bool DeleteInternal(IQueueMessage item)
        {        
            var success = true;
            var client = GetClient();

            //Remove it from the queue as we don't want to see it again
            var deleteMessageRequest = new DeleteMessageRequest();
            deleteMessageRequest.QueueUrl = _queueUrl;
            deleteMessageRequest.ReceiptHandle = ((AmazonSqsQueueMessage)item).QueueMessage.ReceiptHandle;

            try
            {
                var deleteResult = client.DeleteMessageAsync(deleteMessageRequest).Result;
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;
        }        
    }
}

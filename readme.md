λ set NUGET_API_KEY=...

cd src
dotnet build -c Release
dotnet nuget push bin\Release\Tostil.Tools.Queues.AmazonSqs.1.0.2.1.nupkg -k "%NUGET_API_KEY%" -s https://api.nuget.org/v3/index.json